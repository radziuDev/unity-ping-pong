using UnityEngine;

public class wallBehaviour : MonoBehaviour {
    private AudioSource audioSource;
    
    [SerializeField]
    private scoreScript scoreBoard;
    
    private enum WallType {
        LeftWall,
        RightWall,
        Null,
    };

    [SerializeField]
    private WallType wallType = WallType.Null;

    private void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collider) {
        if (collider.gameObject.layer != 3) return;

        if (wallType == WallType.LeftWall || wallType == WallType.RightWall) {
            scoreBoard.UpdateScore(wallType == WallType.LeftWall ? scoreScript.PlayerSide.Right : scoreScript.PlayerSide.Left);
            audioSource.Play();

            if (scoreBoard.scoreLeft > scoreBoard.winScore || scoreBoard.scoreRight < scoreBoard.winScore) collider.gameObject.GetComponent<ballMovment>().Reset(2f);
        }
    }
}
