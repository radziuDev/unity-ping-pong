using UnityEngine;
using TMPro;


public class aiButton : MonoBehaviour {
    [SerializeField]
    private TextMeshProUGUI textMesh;

    private bool isPvP = true;

    public void UpdateText() {
        if (isPvP) textMesh.text = "PvE";
            else textMesh.text = "PvP";
        
        isPvP = !isPvP;
    }
}
