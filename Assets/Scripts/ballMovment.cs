using UnityEngine;

public class ballMovment : MonoBehaviour {
    private Rigidbody2D rb;
    private AudioSource audioSource;
    private Vector2 lastVelocity;

    [SerializeField]
    private float speed = 5f;
    private Vector2 startPosition;

    [SerializeField]
    private GameObject[] players;
    
    private void Start() {
        rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        startPosition = transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.layer == 7) {
            if (lastVelocity.magnitude < 25) lastVelocity *= 1.1f;

            Vector2 currentScale = collision.gameObject.transform.localScale;
            Vector2 newScale = new Vector2(currentScale.x, currentScale.y * 0.9f);
            collision.gameObject.transform.localScale = newScale;
        }

        Vector2 reflectedDirection = Vector2.Reflect(lastVelocity, collision.contacts[0].normal);
        rb.velocity = reflectedDirection;
        lastVelocity = rb.velocity;
        audioSource.Play();
    }

    public void Reset(float invokeTime = 0f) {
        Restart();
        Invoke("InvokedReset", invokeTime);
    }

    public void Restart() {
        CancelInvoke("InvokedReset");
        rb.velocity = new Vector2(0, 0);
        transform.position = startPosition;
    }

    private void InvokedReset() {
        foreach(GameObject player in players) {
            player.transform.localScale = new Vector2(0.2f, 3f);
        }

        Vector2 randomDirection = Random.insideUnitCircle.normalized;
        randomDirection.x = randomDirection.x > 0
            ? Mathf.Clamp(randomDirection.x, 0.5f, 1f) 
            : Mathf.Clamp(randomDirection.x, -1f, -0.5f);
        
        rb.velocity = randomDirection * speed;
        lastVelocity = rb.velocity;
    }
}
