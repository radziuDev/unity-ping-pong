using UnityEngine;

public class playerMovment : MonoBehaviour {
    private Rigidbody2D rb;

    [HideInInspector]
    public bool isAiOn = false; 

    [SerializeField]
    private GameObject ball; 

    [SerializeField]
    private KeyCode inputUp;
    [SerializeField]
    private KeyCode inputDown;

    [SerializeField]
    private float speed = 5f;

    private void Start() {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        rb.velocity = new Vector2(0, 0);

        if (isAiOn) HandleAiMovment();
          else HandlePlayerMovment();
    }

    private void HandlePlayerMovment() {
        if (Input.GetKey(inputUp) || Input.GetKey(inputDown)) {
            Vector2 movement = new Vector2(0, Input.GetKey(inputUp) ? 1 : -1);
            rb.velocity = movement * speed;
        }
    }

    private void HandleAiMovment() {
        Vector3 targetPositionVector = new Vector3(transform.position.x, ball.transform.position.y, 0);
        Vector3 direction = targetPositionVector - transform.position;
        if (direction.magnitude > 1f) rb.velocity = direction.normalized * speed;
    }

    public void AiSwitch() => isAiOn = !isAiOn;
}
