using UnityEngine;
using TMPro;

public class scoreScript : MonoBehaviour {
    private TextMeshProUGUI textMesh;
    
    [SerializeField]
    private GameObject startButton;
    [SerializeField]
    private GameObject aiButton;
    [SerializeField]
    private ballMovment ball;

    [HideInInspector]
    public int winScore = 10;
    [HideInInspector]
    public int scoreLeft = 0;
    [HideInInspector]
    public int scoreRight = 0;

    public enum PlayerSide {
        Left,
        Right,
    };

    private void Start() {
        textMesh = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateScore(PlayerSide side) {
        if (side == PlayerSide.Left) scoreLeft += 1;
            else scoreRight += 1;

        textMesh.text = $"{scoreLeft}   {scoreRight}";

        if (scoreLeft >= winScore) {
            textMesh.text = $"WIN     LOSS";
            Restart();
        }

        if (scoreRight >= winScore) {
            textMesh.text = $"LOSS     WIN";
            Restart();
        }
    }

    private void Restart() {
        startButton.SetActive(true);
        aiButton.SetActive(true);
        ball.Restart();
    }
}
